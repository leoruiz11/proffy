import express from 'express';
import ClassesController from './controllers/ClassesController';
import ConnectionController from './controllers/ConnectionsController';


const routes = express.Router()
const classesControllers = new ClassesController();
const connectionsControllers = new ConnectionController();

interface ScheduleItem {
    week_day: number,
    from: string,
    to: string
}

routes.post('/api/classes', classesControllers.create)
routes.get('/api/classes', classesControllers.index)

routes.post('/api/connections', connectionsControllers.create)
routes.get('/api/connections', connectionsControllers.index)


export default routes